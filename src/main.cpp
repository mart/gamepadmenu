/*
    SPDX-License-Identifier: GPL-2.0-or-later
    SPDX-FileCopyrightText: 2022 Marco Martin <notmart@gmail.com>
*/

#include <QApplication>
#include <QQmlApplicationEngine>
#include <QUrl>
#include <QtQml>

#include "fakeinput.h"
#include "version-gamepadmenu.h"
#include <KAboutData>
#include <KLocalizedContext>
#include <KLocalizedString>

#include "gamepadmenuconfig.h"

Q_DECL_EXPORT int main(int argc, char *argv[])
{
    QGuiApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QApplication app(argc, argv);
    QCoreApplication::setOrganizationName(QStringLiteral("KDE"));
    QCoreApplication::setApplicationName(QStringLiteral("gamepadmenu"));

    KAboutData aboutData(
                         // The program name used internally.
                         QStringLiteral("gamepadmenu"),
                         // A displayable program name string.
                         i18nc("@title", "gamepadmenu"),
                         // The program version string.
                         QStringLiteral(GAMEPADMENU_VERSION_STRING),
                         // Short description of what the app does.
                         i18n("Application Description"),
                         // The license this code is released under.
                         KAboutLicense::GPL,
                         // Copyright Statement.
                         i18n("(c) 2022"));
    aboutData.addAuthor(i18nc("@info:credit", "AUTHOR"), i18nc("@info:credit", "Author Role"), QStringLiteral("notmart@gmail.com"), QStringLiteral("https://yourwebsite.com"));
    KAboutData::setApplicationData(aboutData);

    QQmlApplicationEngine engine;

    auto config = gamepadmenuConfig::self();

    qmlRegisterSingletonInstance("org.kde.gamepadmenu", 1, 0, "Config", config);

    FakeInput input;
    qmlRegisterSingletonInstance("org.kde.gamepadmenu", 1, 0, "Input", &input);

    engine.rootContext()->setContextObject(new KLocalizedContext(&engine));
    engine.load(QUrl(QStringLiteral("qrc:///contents/ui/main.qml")));

    if (engine.rootObjects().isEmpty()) {
        return -1;
    }

    return app.exec();
}
