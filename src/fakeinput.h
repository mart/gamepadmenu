

#pragma once
#include <QObject>
#include <X11/keysymdef.h>
#include <xcb/xtest.h>
#include <xcb/xcb_keysyms.h>

class FakeInput : public QObject {
    Q_OBJECT
public:
    FakeInput(QObject *parent = nullptr);
    xcb_keycode_t getCode(xcb_key_symbols_t *syms, int code);
    Q_INVOKABLE void overview();
    Q_INVOKABLE void maximizeWindow();
    Q_INVOKABLE void closeWindow();
    Q_INVOKABLE void previousWindow();
    Q_INVOKABLE void alt(bool press);
    Q_INVOKABLE void nextWindow();
    Q_INVOKABLE void showLauncher();
    Q_INVOKABLE void showPanel();
    Q_INVOKABLE void showDesktop();
};
