
import QtQuick 2.2
import QtQuick.Controls 2.15 as QQC2
import QtQuick.Layouts 1.1
import QtQuick.Window 2.0
import QtGamepad 1.0

Rectangle {
    property alias text: label.text
    readonly property bool current: parent.currentItem === this
    signal canceled
    signal pressed
    signal released
    Layout.fillWidth: true
    Layout.fillHeight: true
    radius: 5
    color: enabled && current
        ? (gamepad.buttonL3 ? Qt.rgba(0.4,0.4,0.9, 0.5) : Qt.rgba(0,0,0, 0.6))
        : Qt.rgba(0,0,0, 0.3)
    QQC2.Label {
        id: label
        color: "white"
        anchors.fill: parent
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignVCenter
    }
}
