
import QtQuick 2.2
import QtQuick.Controls 2.15 as QQC2
import QtQuick.Layouts 1.1
import QtQuick.Window 2.0
import QtGamepad 1.0
import org.kde.gamepadmenu 1.0

Window {
    id: applicationWindow1
    visible: gamepad.axisLeftX != 0 || gamepad.axisLeftY != 0
    width: 200
    height: 200
    x: 100
    y: 100
    flags: Qt.Popup
    title: qsTr("Gamepad Menu")
    color: "transparent"


    Connections {
        target: GamepadManager
        function onGamepadConnected() {
            gamepad.deviceId = deviceId;
        }
    }

    Gamepad {
        id: gamepad
        deviceId: GamepadManager.connectedGamepads.length > 0 ? GamepadManager.connectedGamepads[0] : -1
        onButtonL3Changed: {
            if (!layout.currentItem) {
                return;
            }
            if (buttonL3) {
                layout.currentItem.pressed()
            } else {
                layout.currentItem.released()
            }
        }
        onButtonAChanged: {
            if (!layout.currentItem) {
                return;
            }
            if (buttonA) {
                layout.currentItem.pressed()
            } else {
                layout.currentItem.released()
            }
        }
    }

    Rectangle {
        id: indicator
        color: "red"
        width: 5
        height: 5
        radius: 5
        z: 1
        x: Math.max(0, Math.min(parent.width - width, parent.width/2 + parent.width/2 * gamepad.axisLeftX - width/2))
        y: Math.max(0, Math.min(parent.height - height, parent.height/2 + parent.height/2 * gamepad.axisLeftY - height/2))
        onXChanged: positionPollTimer.running = true
        onYChanged: positionPollTimer.running = true
        Timer {
            id: positionPollTimer
            interval: 150
            onTriggered: {
                let newItem = layout.childAt(indicator.x + indicator.width/2, indicator.y+indicator.height/2);
                if (layout.currentItem && layout.currentItem != newItem) {
                    layout.currentItem.canceled();
                    if (gamepad.buttonL3) {
                        layout.currentItem = null;
                        return;
                    }
                }
                layout.currentItem = newItem;
            }
        }
    }
    GridLayout {
        id: layout
        anchors.fill: parent
        property Item currentItem
        rows: 3
        columns: 3
        Button {
            text: "Tasks"
            onReleased: Input.overview()
        }
        Button {
            text: "Maximize"
            onReleased: Input.maximizeWindow()
        }
        Button {
            id: closeButton
            text: "Close"
            onPressed: closeAnim.restart()
            onReleased: {
                closeAnim.stop();
                closeProgress.width = 0;
            }
            onCanceled: {
                closeAnim.stop();
                closeProgress.width = 0;
            }
            SequentialAnimation {
                id: closeAnim
                NumberAnimation {
                    target: closeProgress
                    property: "width"
                    from: 0
                    to: closeProgress.parent.width
                    duration: 500
                }
                ScriptAction {
                    script: Input.closeWindow()
                }
            }
            Rectangle {
                id: closeProgress
                color: "red"
                z: -1
                anchors {
                    left: parent.left
                    top: parent.top
                    bottom:parent.bottom
                }
                radius: 5
            }
        }
        Button {
            text: "Previous"
            onCurrentChanged: Input.alt(current)
            onReleased: Input.previousWindow()
        }
        Button {
            enabled: false
        }
        Button {
            text: "Next"
            onCurrentChanged: Input.alt(current)
            onReleased: Input.nextWindow()
        }
        Button {
            text: "Launcher"
            onReleased: Input.showLauncher()
        }
        Button {
            text: "Panel"
            onReleased: Input.showPanel()
        }
        Button {
            text: "Desktop"
            onReleased: Input.showDesktop()
        }
    }
}
