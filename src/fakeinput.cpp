#include <fakeinput.h>
#include <QX11Info>
#define XK_MISCELLANY
#define XK_XKB_KEYS
#include <X11/keysymdef.h>
#include <xcb/xcb_keysyms.h>
#include <xcb/xtest.h>
#define XK_w                             0x0077
#define XK_p                             0x0070  /* U+0070 LATIN SMALL LETTER P */
FakeInput::FakeInput(QObject *parent)
    : QObject(parent)
{
}

xcb_keycode_t FakeInput::getCode(xcb_key_symbols_t *syms, int code)
{
    xcb_keycode_t *keyCodes = xcb_key_symbols_get_keycode(syms, code);
    const xcb_keycode_t ret = keyCodes[0];
    free(keyCodes);
    return ret;
};

void FakeInput::overview()
{
    xcb_connection_t *c = QX11Info::connection();
    xcb_window_t w = QX11Info::appRootWindow();
    xcb_key_symbols_t *syms = xcb_key_symbols_alloc(c);

    xcb_test_fake_input(c, XCB_KEY_PRESS, getCode(syms, XK_Super_L), XCB_TIME_CURRENT_TIME, w, 0, 0, 0);
    xcb_test_fake_input(c, XCB_KEY_PRESS, getCode(syms, XK_w), XCB_TIME_CURRENT_TIME, w, 0, 0, 0);
    xcb_test_fake_input(c, XCB_KEY_RELEASE, getCode(syms, XK_Super_L), XCB_TIME_CURRENT_TIME, w, 0, 0, 0);
    xcb_test_fake_input(c, XCB_KEY_RELEASE, getCode(syms, XK_w), XCB_TIME_CURRENT_TIME, w, 0, 0, 0);
}

void FakeInput::maximizeWindow()
{
    xcb_connection_t *c = QX11Info::connection();
    xcb_window_t w = QX11Info::appRootWindow();
    xcb_key_symbols_t *syms = xcb_key_symbols_alloc(c);

    xcb_test_fake_input(c, XCB_KEY_PRESS, getCode(syms, XK_Super_L), XCB_TIME_CURRENT_TIME, w, 0, 0, 0);
    xcb_test_fake_input(c, XCB_KEY_PRESS, getCode(syms, XK_Page_Up), XCB_TIME_CURRENT_TIME, w, 0, 0, 0);
    xcb_test_fake_input(c, XCB_KEY_RELEASE, getCode(syms, XK_Super_L), XCB_TIME_CURRENT_TIME, w, 0, 0, 0);
    xcb_test_fake_input(c, XCB_KEY_RELEASE, getCode(syms, XK_Page_Up), XCB_TIME_CURRENT_TIME, w, 0, 0, 0);
}

void FakeInput::closeWindow()
{
    xcb_connection_t *c = QX11Info::connection();
    xcb_window_t w = QX11Info::appRootWindow();
    xcb_key_symbols_t *syms = xcb_key_symbols_alloc(c);

    xcb_test_fake_input(c, XCB_KEY_PRESS, getCode(syms, XK_Alt_L), XCB_TIME_CURRENT_TIME, w, 0, 0, 0);
    xcb_test_fake_input(c, XCB_KEY_PRESS, getCode(syms, XK_F4), XCB_TIME_CURRENT_TIME, w, 0, 0, 0);
    xcb_test_fake_input(c, XCB_KEY_RELEASE, getCode(syms, XK_Alt_L), XCB_TIME_CURRENT_TIME, w, 0, 0, 0);
    xcb_test_fake_input(c, XCB_KEY_RELEASE, getCode(syms, XK_F4), XCB_TIME_CURRENT_TIME, w, 0, 0, 0);
}

void FakeInput::alt(bool press)
{
    xcb_connection_t *c = QX11Info::connection();
    xcb_window_t w = QX11Info::appRootWindow();
    xcb_key_symbols_t *syms = xcb_key_symbols_alloc(c);

    if (press) {
        xcb_test_fake_input(c, XCB_KEY_PRESS, getCode(syms, XK_Alt_L), XCB_TIME_CURRENT_TIME, w, 0, 0, 0);
    } else {
        xcb_test_fake_input(c, XCB_KEY_RELEASE, getCode(syms, XK_Alt_L), XCB_TIME_CURRENT_TIME, w, 0, 0, 0);
    }
}

void FakeInput::previousWindow()
{
    xcb_connection_t *c = QX11Info::connection();
    xcb_window_t w = QX11Info::appRootWindow();
    xcb_key_symbols_t *syms = xcb_key_symbols_alloc(c);

    xcb_test_fake_input(c, XCB_KEY_PRESS, getCode(syms, XK_Shift_L), XCB_TIME_CURRENT_TIME, w, 0, 0, 0);
    xcb_test_fake_input(c, XCB_KEY_PRESS, getCode(syms, XK_Tab), XCB_TIME_CURRENT_TIME, w, 0, 0, 0);
    xcb_test_fake_input(c, XCB_KEY_RELEASE, getCode(syms, XK_Shift_L), XCB_TIME_CURRENT_TIME, w, 0, 0, 0);
    xcb_test_fake_input(c, XCB_KEY_RELEASE, getCode(syms, XK_Tab), XCB_TIME_CURRENT_TIME, w, 0, 0, 0);
}

void FakeInput::nextWindow()
{
    xcb_connection_t *c = QX11Info::connection();
    xcb_window_t w = QX11Info::appRootWindow();
    xcb_key_symbols_t *syms = xcb_key_symbols_alloc(c);

    xcb_test_fake_input(c, XCB_KEY_PRESS, getCode(syms, XK_Tab), XCB_TIME_CURRENT_TIME, w, 0, 0, 0);
    xcb_test_fake_input(c, XCB_KEY_RELEASE, getCode(syms, XK_Tab), XCB_TIME_CURRENT_TIME, w, 0, 0, 0);
}

void FakeInput::showLauncher()
{
    xcb_connection_t *c = QX11Info::connection();
    xcb_window_t w = QX11Info::appRootWindow();
    xcb_key_symbols_t *syms = xcb_key_symbols_alloc(c);

    xcb_test_fake_input(c, XCB_KEY_PRESS, getCode(syms, XK_Super_L), XCB_TIME_CURRENT_TIME, w, 0, 0, 0);
    xcb_test_fake_input(c, XCB_KEY_RELEASE, getCode(syms, XK_Super_L), XCB_TIME_CURRENT_TIME, w, 0, 0, 0);
}

void FakeInput::showPanel()
{
    xcb_connection_t *c = QX11Info::connection();
    xcb_window_t w = QX11Info::appRootWindow();
    xcb_key_symbols_t *syms = xcb_key_symbols_alloc(c);

    xcb_test_fake_input(c, XCB_KEY_PRESS, getCode(syms, XK_Super_L), XCB_TIME_CURRENT_TIME, w, 0, 0, 0);
    xcb_test_fake_input(c, XCB_KEY_PRESS, getCode(syms, XK_Alt_L), XCB_TIME_CURRENT_TIME, w, 0, 0, 0);
    xcb_test_fake_input(c, XCB_KEY_PRESS, getCode(syms, XK_p), XCB_TIME_CURRENT_TIME, w, 0, 0, 0);
    xcb_test_fake_input(c, XCB_KEY_RELEASE, getCode(syms, XK_Super_L), XCB_TIME_CURRENT_TIME, w, 0, 0, 0);
    xcb_test_fake_input(c, XCB_KEY_RELEASE, getCode(syms, XK_Alt_L), XCB_TIME_CURRENT_TIME, w, 0, 0, 0);
    xcb_test_fake_input(c, XCB_KEY_RELEASE, getCode(syms, XK_p), XCB_TIME_CURRENT_TIME, w, 0, 0, 0);
}

void FakeInput::showDesktop()
{
    xcb_connection_t *c = QX11Info::connection();
    xcb_window_t w = QX11Info::appRootWindow();
    xcb_key_symbols_t *syms = xcb_key_symbols_alloc(c);

    xcb_test_fake_input(c, XCB_KEY_PRESS, getCode(syms, XK_Control_L), XCB_TIME_CURRENT_TIME, w, 0, 0, 0);
    xcb_test_fake_input(c, XCB_KEY_PRESS, getCode(syms, XK_F12), XCB_TIME_CURRENT_TIME, w, 0, 0, 0);
    xcb_test_fake_input(c, XCB_KEY_RELEASE, getCode(syms, XK_Control_L), XCB_TIME_CURRENT_TIME, w, 0, 0, 0);
    xcb_test_fake_input(c, XCB_KEY_RELEASE, getCode(syms, XK_F12), XCB_TIME_CURRENT_TIME, w, 0, 0, 0);
}

#include "moc_fakeinput.cpp"
